angular.module("app", ['ngMaterial', 'ngResource'])

.controller("TodoList", ['$scope', 'listService', function($scope, listService){

    //creation de la liste
    var list = [];
    var tmpList = [];
    function initList(){
        list = [];
        tmpList = [];

        listService.query().$promise.then(
            function (data) {
                var nb = data.length;

                for (var c = 0; c<nb ; c++) {
                    list.push({"text": data[c].title, "statut": data[c].completed, "id":data[c].id});
                }

                /*tmpList = list;
                 $scope.list = tmpList;
                 $scope.getNbPages();
                 $scope.getListEnCours();*/
                $scope.getSearch();
            }
        );
    }
    initList();


    //init variable pour pagination
        $scope.currentPage = 0;
        $scope.numPerPage = 5;
        $scope.currentObj = {};

    //recuperation du nombre de page
    var nbPages;
    $scope.getNbPages = function() {
        $scope.maxSizePage = Math.ceil($scope.list.length / $scope.numPerPage);
        var nbPages = [];
        var c = 0;
        for (i=0 ; i < $scope.maxSizePage ; i++) {
            c++;
            nbPages.push(i+1);
        }
        $scope.nbPages = nbPages;

        if($scope.currentPage > c){
            $scope.currentPage = 0;
        }
    };

    //attribution du tableau en cours
    var listEnCours;
    $scope.getListEnCours = function() {
        listEnCours = [];

        for (i= Math.floor($scope.currentPage * $scope.numPerPage) ; i < Math.floor($scope.currentPage * $scope.numPerPage) + 5 ; i++) {

            if(i < $scope.list.length){
                listEnCours.push($scope.list[i]);
            }

        }

        console.log(listEnCours);

        $scope.listEnCours = listEnCours;
    };

    $scope.edit = undefined;
    $scope.check = false;

    $scope.addInput = function (){
        listService.save({"title": $scope.currentObj.text, "completed": false, "userId": 4}).$promise.then(function (data) {
            list.push({"text": data.title, "statut": data.completed, "id":data.id});
            $scope.getSearch();
        });

        /*$scope.getNbPages();
        $scope.getListEnCours();*/
        $scope.getSearch();
    };

    $scope.deleteInput = function (it){
        $scope.currentObj = tmpList[$scope.numPerPage * $scope.currentPage + it];

        listService.remove({"id": $scope.currentObj.id}).$promise.then(function () {

            //reconstruire tableau
            initList();
        });
    };

    $scope.editInput = function (i){
        $scope.currentObj = tmpList[$scope.numPerPage * $scope.currentPage + i];
    };

    $scope.validEditInput = function (id){
        listService.get({id:$scope.currentObj.id}).$promise.then(
            function (data) {
                data.title = $scope.currentObj.text;
                data.completed = $scope.currentObj.statut;

                listService.update({id:$scope.currentObj.id}, data).$promise.then(function (data) {
                    $scope.getSearch();
                    $scope.currentObj = {};
                });
            }
        );
    };

    $scope.closeEdit = function (){
        $scope.currentObj = {};
    };

    $scope.goToPage = function (it){
        $scope.currentPage = it;
        $scope.getNbPages();
        $scope.getListEnCours();
    };

    var nbSearch = 0;
    $scope.getSearch = function (){
        tmpList = [];

        if($scope.search === undefined){
            nbSearch = 0;
        }else{
            nbSearch = $scope.search.length;
        }

        if(nbSearch > 0){

            for (i= 0 ; i <  list.length ; i++) {

                if(list[i].text.substr(0, nbSearch).toLowerCase() === $scope.search ){
                    tmpList.push(list[i]);
                }
            }

        }else{
            tmpList = list;
        }

        $scope.list = tmpList;
        $scope.getNbPages();
        $scope.getListEnCours();
    };
}])

.directive('listDirective', function(){
    return{
        restrict : 'EA',
        replace : true,
        scope: {edit: "=edit", listExpended: '=listExpended', key : "=dKey", value : "=dValue"},
        templateUrl : 'layout/list.html'
    }
})

.factory('listService', ['$resource',function ($resource) {
    return $resource('http://localhost:3000/todos/:id', null,
        {
            'get':    {method:'GET'},
            'save':   {method:'POST'},
            'query':  {method:'GET', isArray:true},
            'remove': {method:'DELETE'},
            'delete': {method:'DELETE'},
            'update': {method:'PUT'}
        });
}])
;
