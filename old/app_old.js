angular.module("app", ['ngMaterial', 'ngResource'])

.controller("TodoList", ['$scope', 'listService', function($scope, listService){

    //creation de la liste
    var list = [];
    var tmpList = [];

    listService.query().$promise.then(
        function (data) {
            console.log(data[0]);

            var nb = data.length;

            for (var c = 0; c<nb ; c++) {
                list.push({"text": data[c].title, "statut": true, id:data[c].id});
            }

            tmpList = list;
            $scope.list = tmpList;
            $scope.getNbPages();
            $scope.getListEnCours();
        }
    );

    /*$scope.makeTodos = function() {
        for (i=0;i<10;i++) {
            list.push({"text": "Petiprez", "statut": true});
        }
    };
    tmpList = list;
    $scope.list = tmpList;
    $scope.makeTodos();*/

    //init variable pour pagination
        $scope.currentPage = 0;
        $scope.numPerPage = 5;

    //recuperation du nombre de page
    var nbPages;
    $scope.getNbPages = function() {
        $scope.maxSizePage = Math.ceil($scope.list.length / $scope.numPerPage);

        console.log($scope.maxSizePage + "/" + $scope.list.length);

        var nbPages = [];
        for (i=0 ; i < $scope.maxSizePage ; i++) {
            nbPages.push(i+1);
        }
        $scope.nbPages = nbPages;
    };

    //attribution du tableau en cours
    var listEnCours;
    $scope.getListEnCours = function() {
        listEnCours = [];

        console.log($scope.list.length);

        for (i= Math.floor($scope.currentPage * $scope.numPerPage) ; i < Math.floor($scope.currentPage * $scope.numPerPage) + 5 ; i++) {

            console.log('for : ' + i);

            if(i < $scope.list.length){
                listEnCours.push($scope.list[i]);
            }

        }

        console.log("liste en cours");
        console.log(listEnCours);
        $scope.listEnCours = listEnCours;
    };

    $scope.edit = undefined;
    $scope.check = false;

    $scope.addInput = function (){
        list.push({"text": $scope.yourName, "statut": false});

        /*$scope.getNbPages();
        $scope.getListEnCours();*/
        $scope.getSearch();
    };

    $scope.deleteInput = function (it){

        var element = tmpList[$scope.numPerPage * $scope.currentPage + it];

        for (var i = 0; i < list.length; i++){
            if(JSON.stringify(element) === JSON.stringify(list[i])){
                console.log(list[i]);
                list.splice(i, 1);
            }
        }

        /*$scope.getNbPages();
        $scope.getListEnCours();*/
        $scope.getSearch();
    };

    $scope.editInput = function (i){
        $scope.yourName = list[$scope.numPerPage * $scope.currentPage + i].text;
        $scope.edit = $scope.numPerPage * $scope.currentPage + i;
        $scope.check = list[$scope.numPerPage * $scope.currentPage + i].statut;
        tmpList = list;
    };

    $scope.validEditInput = function (){
        if($scope.check){
            list[$scope.edit].statut = true;
        }else{
            list[$scope.edit].statut = false;
        }

        list[$scope.edit].text = $scope.yourName;
        $scope.edit = undefined;
        tmpList = list;
    };

    $scope.closeEdit = function (){
        $scope.edit = undefined;
    };

    $scope.goToPage = function (it){
        $scope.currentPage = it;
        $scope.getNbPages();
        $scope.getListEnCours();
    };

    var nbSearch = 0;
    $scope.getSearch = function (){
        tmpList = [];

        if($scope.search === undefined){
            nbSearch = 0;
        }else{
            nbSearch = $scope.search.length;
        }

        if(nbSearch > 0){

            for (i= 0 ; i <  list.length ; i++) {

                if(list[i].text.substr(0, nbSearch).toLowerCase() === $scope.search ){
                    tmpList.push(list[i]);
                }
            }
        }else{
            tmpList = list;
        }

        $scope.list = tmpList;
        //console.log($scope.list);
        $scope.getNbPages();
        $scope.getListEnCours();
    };

    $scope.test = function() {

    };
}])

.directive('listDirective', function(){
    return{
        restrict : 'EA',
        replace : true,
        scope: {edit: "=edit", listExpended: '=listExpended', key : "=dKey", value : "=dValue"},
        templateUrl : '../layout/list.html'
    }
})

.factory('listService', ['$resource',function ($resource) {
    return $resource('https://jsonplaceholder.typicode.com/posts/:id', null,
        {
            'get':    {method:'GET'},
            'save':   {method:'POST'},
            'query':  {method:'GET', isArray:true},
            'remove': {method:'DELETE'},
            'delete': {method:'DELETE'}
        });
}])
;
